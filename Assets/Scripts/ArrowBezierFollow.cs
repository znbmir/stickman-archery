﻿using System.Collections;
using UnityEngine;

public class ArrowBezierFollow : MonoBehaviour
{
    [SerializeField]
    private Transform routes;
    private int routeToGo;
    private float tParam;
    private Vector3 arrowPosition;
    private float speedModifier;
    private bool coroutineAllowed;
    public static bool shootAllowed;
    private Quaternion defaultArrowRotation;
    public float power = 50.0f;



    // Start is called before the first frame update
    void Start()
    {

    }

    private void Awake()
    {
        routeToGo = 0;
        tParam = 0f;
        speedModifier = 0.5f;
        coroutineAllowed = true;
        shootAllowed = false;
        defaultArrowRotation = this.transform.localRotation;
        routes = GameManager.Instance.controlPoints;

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (shootAllowed && coroutineAllowed)
        {
            StartCoroutine(GoByTheRoute(routeToGo));
            this.transform.GetChild(0).GetComponent<ParticleSystem>().Play();

        }

        if (this.transform.position.z > 200)
            Destroy(this.gameObject);
    }

    private IEnumerator GoByTheRoute(int routeNumber)
    {
        coroutineAllowed = false;

        Vector3 p0 = routes.GetChild(0).position;
        Vector3 p1 = routes.GetChild(1).position;
        Vector3 p2 = routes.GetChild(2).position;
        Vector3 p3 = routes.GetChild(3).position;

        while(tParam < 1)
        {
            tParam += Time.deltaTime * speedModifier;

            arrowPosition = Mathf.Pow(1 - tParam, 3) * p0 +
                3 * Mathf.Pow(1 - tParam, 2) * tParam * p1 +
                3 * (1 - tParam) * Mathf.Pow(tParam, 2) * p2 +
                Mathf.Pow(tParam, 3) * p3;

            transform.rotation = Quaternion.LookRotation(arrowPosition - this.transform.position);
            this.transform.position = arrowPosition;
            yield return new WaitForEndOfFrame();
        }

        tParam = 0f;
        routeToGo += 1;

        if (routeToGo > 0)
        {
            routeToGo = 0;
            shootAllowed = false;


        }

        coroutineAllowed = true;
    }




}
