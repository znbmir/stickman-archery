﻿using UnityEngine;
using TMPro;

public class ArrowUpdate : MonoBehaviour
{
    public TextMeshProUGUI textNum_arrowGolden;
    public TextMeshProUGUI textNum_arrowSilver;
    public TextMeshProUGUI textNum_arrowBronze;

    // Start is called before the first frame update
    void Update()
    {
        if (GameManager.Instance.arrowNumUpdated)
        {
            GameManager.Instance.arrowNumUpdated = false;
            textNum_arrowGolden.text = "" + GameConstants.ARROWGOLDEN;
       
            textNum_arrowSilver.text = "" + GameConstants.ARROWSILVER;

            textNum_arrowBronze.text = "" + GameConstants.ARROWBRONZE;
        }
    }
}
