﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barill : MonoBehaviour
{
    private List<GameObject> list = new List<GameObject>();
    public bool barrilExplode = false;
    private float power = 3.0f;
    private float upForce = 0.8f;
    private Vector3 explosionPosition;
    private float radius = 10.0f;
    private TimeManager timeManager;
    public bool barillInDanger;

    private ParticleSystem ps;

    private void Awake()
    {
        ps = this.transform.GetChild(0).GetComponent<ParticleSystem>();
        timeManager = GameManager.Instance.timeManager;
        radius = this.transform.parent.GetComponent<CapsuleCollider>().radius;
        explosionPosition = this.transform.parent.position;


    }
    private void Update()
    {
        if (ps)
        {
            if (barrilExplode && !ps.IsAlive())
            {
                Destroy(this.transform.GetChild(0).gameObject);
            }
        }

        //   if (barrilExplode == true && !timeManager.slowmotionFinished && this.transform.childCount > 0)
        //   {
        //       timeManager.StartCoroutine(timeManager.DoSlowmotion());
        //       for (int i = 0; i < list.Count; i++)
        //       {
        //           list[i].transform.GetComponent<IfInDanger>().radiusExplosion = this.transform.GetComponent<CapsuleCollider>().radius;
        //           list[i].transform.GetComponent<IfInDanger>().explosionPosition = explosionPosition;
        //           list[i].transform.GetComponent<IfInDanger>().KillEnemyAoundBarill();
        //
        //       //   list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>().isAlive = false;
        //       //
        //       //   list[i].GetComponentInParent<Animator>().enabled = false;
        //       //
        //       //   if (list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>())
        //       //       list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>().ActivateRagdoll();
        //       //   list[i].GetComponentInParent<Animator>().SetTrigger("dying");
        //       //
        //       //   list[i].GetComponent<Rigidbody>().velocity = Vector3.zero;
        //       //   list[i].GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        //       //
        //       //   if (GameManager.Instance.notWinYet)
        //       //       GameManager.Instance.TestForWin();
        //
        //       }
        //
        //       barrilExplode = false;
        //   }

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Arrow")
        {
            this.transform.GetComponent<BoxCollider>().enabled = false;
            this.transform.GetChild(0).GetComponent<ParticleSystem>().Play();
            this.transform.GetComponent<MeshRenderer>().enabled = false;
            barrilExplode = true;
            this.GetComponentInParent<BarrilEnemy>().GetComponent<CapsuleCollider>().enabled = false;
            GameManager.Instance.cam.GetComponent<Animator>().SetTrigger("shake");

            if( this.transform.parent.childCount > 0)
            {
                timeManager.StartCoroutine(timeManager.DoSlowmotion());

                for (int i = 0; i < this.transform.parent.GetComponent<BarrilEnemy>().list.Count; i++)
                {
                    this.transform.parent.GetComponent<BarrilEnemy>().list[i].transform.GetComponent<IfInDanger>().radiusExplosion = radius;
                    this.transform.parent.GetComponent<BarrilEnemy>().list[i].transform.GetComponent<IfInDanger>().explosionPosition = explosionPosition;
                    this.transform.parent.GetComponent<BarrilEnemy>().list[i].transform.GetComponent<IfInDanger>().KillEnemyAoundBarill();

                    //   list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>().isAlive = false;
                    //
                    //   list[i].GetComponentInParent<Animator>().enabled = false;
                    //
                    //   if (list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>())
                    //       list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>().ActivateRagdoll();
                    //   list[i].GetComponentInParent<Animator>().SetTrigger("dying");
                    //
                    //   list[i].GetComponent<Rigidbody>().velocity = Vector3.zero;
                    //   list[i].GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                    //
                    //   if (GameManager.Instance.notWinYet)
                    //       GameManager.Instance.TestForWin();

                }
            }
        }


        if (col.gameObject.CompareTag("DangerPath"))
        {
            this.GetComponentInParent<BarrilEnemy>().barillInDanger = true;
        }

    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.CompareTag("DangerPath"))
        {
            this.GetComponentInParent<BarrilEnemy>().barillInDanger = false;
        }
    }
}
