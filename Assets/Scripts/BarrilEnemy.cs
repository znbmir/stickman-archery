﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrilEnemy : MonoBehaviour
{

    public List<GameObject> list = new List<GameObject>();
    private float power = 3.0f;
    private float upForce = 0.8f;
    //private Vector3 explosionPosition;
    //private float radius = 10.0f;
    //private TimeManager timeManager;
    public bool barillInDanger;

    private void Awake()
    {
        //timeManager = GameManager.Instance.timeManager;
        //explosionPosition = this.transform.position;

        //radius = this.transform.GetComponent<CapsuleCollider>().radius;
    }
    private void OnDisable()
    {
        list.Clear();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            list.Add(col.gameObject);
        }

    }   

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            col.gameObject.transform.GetComponent<SkinnedMeshRenderer>().material = GameManager.Instance.enemyRedMaterial;
            list.Remove(col.gameObject);
        }
    }

    private void Update()
    {
        if (barillInDanger)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>().isAlive)
                    list[i].transform.GetComponent<SkinnedMeshRenderer>().material = GameManager.Instance.enemyOrangeMaterial;
                else
                    list.Remove(list[i]);

            }
        }


    //   if (barrilExplode == true && !timeManager.slowmotionFinished && this.transform.childCount > 0)
    //   {
    //       timeManager.StartCoroutine(timeManager.DoSlowmotion());
    //       for (int i = 0; i < list.Count; i++)
    //       {
    //           list[i].transform.GetComponent<IfInDanger>().radiusExplosion = this.transform.GetComponent<CapsuleCollider>().radius;
    //           list[i].transform.GetComponent<IfInDanger>().explosionPosition = explosionPosition;
    //           list[i].transform.GetComponent<IfInDanger>().KillEnemyAoundBarill();
    //
    //       //   list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>().isAlive = false;
    //       //
    //       //   list[i].GetComponentInParent<Animator>().enabled = false;
    //       //
    //       //   if (list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>())
    //       //       list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>().ActivateRagdoll();
    //       //   list[i].GetComponentInParent<Animator>().SetTrigger("dying");
    //       //
    //       //   list[i].GetComponent<Rigidbody>().velocity = Vector3.zero;
    //       //   list[i].GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    //       //
    //       //   if (GameManager.Instance.notWinYet)
    //       //       GameManager.Instance.TestForWin();
    //
    //       }
    //
    //       barrilExplode = false;
    //   }
        if (!Input.GetMouseButton(0))
            barillInDanger = false;


        if (!barillInDanger)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].transform.parent.GetChild(0).GetComponent<RagdollControl>().isAlive)
                    list[i].transform.GetComponent<SkinnedMeshRenderer>().material = GameManager.Instance.enemyRedMaterial;
            }
        }

    }

}