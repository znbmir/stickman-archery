﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawRoute : MonoBehaviour
{
    [SerializeField]
    private Vector3 arrowPosition;
    public static bool pointsMoved;


    // Start is called before the first frame update
    void Awake()
    {
        pointsMoved = true;
    }


    // Update is called once per frame
    void Update()
    {
        GoByTheRoute();
    }

    //private IEnumerator GoByTheRoute()
    private void GoByTheRoute()
    {
        pointsMoved = false;

        Vector3 p0 = GameManager.Instance.routes.GetChild(0).position;
        Vector3 p1 = GameManager.Instance.routes.GetChild(1).position;
        Vector3 p2 = GameManager.Instance.routes.GetChild(2).position;
        Vector3 p3 = GameManager.Instance.routes.GetChild(3).position;
        int i = 0;

        GameManager.Instance.pathObj[0].transform.rotation = Quaternion.LookRotation(GameManager.Instance.pathObj[1].transform.position - GameManager.Instance.pathObj[0].transform.position);

        for (float t = 0; t <= 1; t += 0.05f)
        {
            arrowPosition = Mathf.Pow(1 - t, 3) * p0 +
                3 * Mathf.Pow(1 - t, 2) * t * p1 +
                3 * (1 - t) * Mathf.Pow(t, 2) * p2 +
                Mathf.Pow(t, 3) * p3;

            if (i > 0)
            {
                GameManager.Instance.pathObj[i].transform.rotation = Quaternion.LookRotation(arrowPosition - GameManager.Instance.pathObj[i - 1].transform.position);
                if (GameManager.Instance.pathObjMoving[i - 1].transform.position.z < 4)
                    GameManager.Instance.pathObjMoving[i - 1].transform.rotation = GameManager.Instance.pathObj[i - 1].transform.rotation;
                else
                    GameManager.Instance.pathObjMoving[i - 1].transform.rotation = GameManager.Instance.pathObj[i].transform.rotation;
                //GameManager.Instance.pathObjMoving[i-1].transform.rotation = Quaternion.Lerp(GameManager.Instance.pathObj[i-1].transform.rotation, GameManager.Instance.pathObj[i].transform.rotation, Time.deltaTime * 0.5f); ;

            }
            GameManager.Instance.pathObj[i].transform.position = arrowPosition;
            GameManager.Instance.pathObjMoving[i].transform.position = arrowPosition;

            i++;
        }
        GameManager.Instance.pathObjMoving[GameManager.Instance.pathObj.Length - 1].transform.rotation =
            GameManager.Instance.pathObj[GameManager.Instance.pathObj.Length - 1].transform.rotation;

    }
}
