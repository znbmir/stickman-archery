﻿using UnityEngine;

public class FreezeRotation : MonoBehaviour
{
    private Quaternion iniRot;

    void Start()
    {
        iniRot = this.transform.rotation;
    }

    void LateUpdate()
    {
        this.transform.rotation = iniRot;
    }
}
