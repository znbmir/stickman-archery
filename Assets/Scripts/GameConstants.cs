using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConstants : MonoBehaviour
{
    public static int SCORE = 0;
    public static int COUNTSPEED = 70;
    public static int SHOT = 0;
    public static int LEVEL = 0;
    public static int ARROWGOLDEN = 0;
    public static int ARROWSILVER = 0;
    public static int ARROWBRONZE = 0;
}
