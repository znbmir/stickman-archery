﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject playerParent;
    public GameObject playerCharacter;
    public Player _player;

    public GameObject arrowPrefab;
    public GameObject arrowParent;
    public GameObject arrowMovingParent;

    public Transform controlPoints;
    public GameObject arrow;

    public bool instantiateArrow;
    public bool hasArrow;
    public Camera cam;

    public GameObject[] pathObj;
    public GameObject[] pathObjMoving;

    public TimeManager timeManager;
    public Transform routes;

    public GameObject enemyPrefab;
    public GameObject enemyParent;
    public Material enemyRedMaterial;
    public Material enemyOrangeMaterial;


    public GameObject barilPrefab;
    public GameObject barilParent;

    public LevelDesign levelDesign;
    public int levelDifficulty;
    public List<List<SpawnPosition>> spawnPositionIdle = new List<List<SpawnPosition>>();//[9, 10]
    public float[] dxIdle = new float[] { -20f, -15f, -10f, -5f, 0f, 5f, 10f, 15f, 20f };//dx: [-32, -20, -10, 0, 10, 20, 32] for idle enemy
    public float[] dz = new float[] { 0.1f, 0.15f, 0.19f, 0.23f, 0.26f, 0.29f, 0.31f, 0.33f, 0.36f, 0.39f };//enemyPosArray: 0.1f, 0.15, 0.2, 0.25, 0.30, 0.35, 0.40, 0.45, 0.5, 0.55f]

    public bool arrowNumUpdated;

    public bool notWinYet = true;
    public GameObject victoryEffect;
    public bool scoreUpdated;
    public GameObject coinNumCanvas;
    public GameObject shootTxt;

    public GameObject greatTxt;
    public GameObject goodTxt;
    public GameObject fineTxt;

    //**************************************************Level Parameters********************************************
    public int numOrbitEnemyBarillInside = 1;//number of different path
    int numOrbitBarillInside = 1;// number of orbit in each path
    int numEnemyEachOrbitBarillInside = 5;
    float radiusOrbitBarillInside = 5f;
    float speedOrbitBarillInside = 100f;

    public int numOrbitEnemyBarillOutside = 3;
    int numOrbitBarillOutside = 1;
    int numEnemyEachOrbitBarillOutside = 3;
    float radiusOrbitBarillOutside = 5f;
    float speedOrbitBarillOutside = 100f;

    public int numRandomEnemy = 0;
    float radiusRandomEnemy = 3f;
    float speedRandomEnemy = 7f;

    public int numIdleEnemyPath = 0;
    int numIdleEnemyEachPath = 4;
    //***************************************************************************************************************



    private static GameManager _instance = null;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameManager();
            }
            return _instance;
        }
    }

    private GameManager()
    {
        _instance = this;
    }

    private void Awake()
    {
        playerCharacter = Instantiate(playerPrefab, playerParent.transform.position, Quaternion.identity);
        playerCharacter.transform.SetParent(playerParent.transform.parent, false);

        _player.player = playerCharacter;
        _player.PlayerAnim = playerCharacter.GetComponent<Animator>();

        arrowParent = playerCharacter.transform.GetChild(0).GetChild(0).gameObject;

        instantiateArrow = true;

        foreach (GameObject obj in GameManager.Instance.pathObjMoving)
            obj.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

        foreach (GameObject obj in GameManager.Instance.pathObj)
            obj.transform.GetChild(0).GetComponent<CapsuleCollider>().enabled = false;

        LevelInitializtion();

    }


    // Update is called once per frame
    void Update()
    {
  
        if (instantiateArrow && notWinYet)
        {
            StartCoroutine(InstantiateArrow());
            instantiateArrow = false;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetLevel();
        }
    }


    IEnumerator InstantiateArrow()
    {
        yield return new WaitForSeconds(0.7f);
        arrow = Instantiate(arrowPrefab);
        arrow.transform.SetParent(arrowParent.transform.parent, false);
        arrow.AddComponent<ArrowBezierFollow>();
        hasArrow = true;
    }
    //******************************************************************************Level Initialization************************************************************************
    public void LevelInitializtion()
    {
        GameConstants.LEVEL = PlayerPrefs.GetInt("LEVEL", 0) + 1;
        GameConstants.SCORE = PlayerPrefs.GetInt("SCORE", 0);

        for (int i = 0; i < dxIdle.Length; i++)
        {
            spawnPositionIdle.Add(new List<SpawnPosition>());

            for (int j = 0; j < dz.Length; j++)
            {
                SpawnPosition sp = new SpawnPosition(0f, 0f, false);
                spawnPositionIdle[i].Add(sp);
            }
        }

        for (int i = 0; i < dxIdle.Length; i++)
            for (int j = 0; j < dz.Length; j++)
            {
                spawnPositionIdle[i][j].x = dxIdle[i];
                spawnPositionIdle[i][j].z = dz[j];
                spawnPositionIdle[i][j].positionStatus = false;
            }


        levelDifficulty = levelDesign.GetLineSplitDifficulty();

        DefineParameterForEachDifficulty();



        //Orbit Enemy Barill Inside
      for (int i = 0; i < numOrbitEnemyBarillInside; i++)
      {
          SpawnMovingEnemy(numOrbitBarillInside, numEnemyEachOrbitBarillInside, radiusOrbitBarillInside, speedOrbitBarillInside, true);
      }
     
     
        //Orbit Enemy Barill Outside
        for (int i = 0; i < numOrbitEnemyBarillOutside; i++)
        {
            SpawnMovingEnemy(numOrbitBarillOutside, numEnemyEachOrbitBarillOutside, radiusOrbitBarillOutside, speedOrbitBarillOutside, false);
        }

        //Idle Enemy
        for (int i = 0; i < numIdleEnemyPath; i++)
        {
            SpawnIdleEnemy(numIdleEnemyEachPath);
        }
    
        //Random Enemy
        for (int i = 0; i < numRandomEnemy; i++)
        {
            SpawnRandomEnemy(radiusRandomEnemy, speedRandomEnemy);
        }

        GameConstants.ARROWGOLDEN = numOrbitEnemyBarillInside + numOrbitEnemyBarillOutside + numIdleEnemyPath + numRandomEnemy;
        GameConstants.ARROWSILVER = numOrbitEnemyBarillInside + numOrbitEnemyBarillOutside + numIdleEnemyPath + numRandomEnemy + (int)levelDifficulty / 5 + 2;
        GameConstants.ARROWBRONZE = numOrbitEnemyBarillInside + numOrbitEnemyBarillOutside + numIdleEnemyPath + numRandomEnemy + (int)levelDifficulty / 5 + 4;
        arrowNumUpdated = true;
    }


    //************************************************************************Random*****************************************************************************************
    public void SpawnRandomEnemy(float radiusRandom, float speed)
    {
        List<float> enemyPosArray = new List<float>();
        float dxBorderL;
        float dxBorderR;


        int dxIndexEmptyPlace = 1000;
        int dzIndexEmptyPlace = 1000;

        Vector2 temp = FindEmptyPlace(1, radiusRandom);

        dxIndexEmptyPlace = (int)temp.x;
        dzIndexEmptyPlace = (int)temp.y;

        if (dxIndexEmptyPlace < 1000)//there is Empty place
        {

            enemyPosArray.Add(spawnPositionIdle[dxIndexEmptyPlace][dzIndexEmptyPlace].z);

            float dx = spawnPositionIdle[dxIndexEmptyPlace][dzIndexEmptyPlace].x;
            dxBorderL = spawnPositionIdle[dxIndexEmptyPlace - ((int)(radiusRandom - 1) / 2)][dzIndexEmptyPlace].x;
            dxBorderR = spawnPositionIdle[dxIndexEmptyPlace + ((int)(radiusRandom - 1) / 2)][dzIndexEmptyPlace].x;
            SpawnRandomEnemyAlongTheRoute(1, radiusRandom, speed, enemyPosArray, dx, dxBorderL, dxBorderR);
        }
    }


    //******************************************************Spawn Random Enemy
    public void SpawnRandomEnemyAlongTheRoute(int numEnemy, float radiusRandom, float speedRandom, List<float> enemyPos, float dx, float dxBorderL, float dxBorderR)
    {
        List<Vector3> point = new List<Vector3>();
        List<Vector3> borderPointL = new List<Vector3>();
        List<Vector3> borderPointR = new List<Vector3>();
        Vector3 startPosition = new Vector3();
        Vector3 targetPosition = new Vector3();
        Vector3 p0 = GameManager.Instance.routes.GetChild(0).position;
        Vector3 p1 = GameManager.Instance.routes.GetChild(1).position;
        Vector3 p2 = GameManager.Instance.routes.GetChild(2).position;
        Vector3 p3 = GameManager.Instance.routes.GetChild(3).position;

        //************************Border pointL
        for (int i = 0; i < numEnemy; i++)
        {
            p1.x = dxBorderL;
            p2.x = dxBorderL;
            float t1 = enemyPos[i];
            if (t1 < 1f)
            {
                borderPointL.Add(Mathf.Pow(1 - t1, 3) * p0 +
                        3 * Mathf.Pow(1 - t1, 2) * t1 * p1 +
                        3 * (1 - t1) * Mathf.Pow(t1, 2) * p2 +
                        Mathf.Pow(t1, 3) * p3);
            }
            var tempL = borderPointL[i];
            tempL.y = 0;
            borderPointL[i] = tempL;

        }



        //************************Baril pointR
        for (int i = 0; i < numEnemy; i++)
        {
            p1.x = dxBorderR;
            p2.x = dxBorderR;
            float t2 = enemyPos[i];
            if (t2 < 1f)
            {
                borderPointR.Add(Mathf.Pow(1 - t2, 3) * p0 +
                        3 * Mathf.Pow(1 - t2, 2) * t2 * p1 +
                        3 * (1 - t2) * Mathf.Pow(t2, 2) * p2 +
                        Mathf.Pow(t2, 3) * p3);
            }
            var tempR = borderPointR[i];
            tempR.y = 0;
            borderPointR[i] = tempR;

        }

        for (int i = 0; i < numEnemy; i++)
        {
            p1.x = dx;
            p2.x = dx;

            float t = enemyPos[i];
            if (t < 1f)
            {
                point.Add(Mathf.Pow(1 - t, 3) * p0 +
                        3 * Mathf.Pow(1 - t, 2) * t * p1 +
                        3 * (1 - t) * Mathf.Pow(t, 2) * p2 +
                        Mathf.Pow(t, 3) * p3);
            }

            var temp = point[i];
            temp.y = 0;
            point[i] = temp;

            startPosition = point[i];
            float radiusRandomNew = Mathf.Abs(borderPointR[i].x - borderPointL[i].x) / 2;
            targetPosition.x = Random.Range(startPosition.x - radiusRandomNew, startPosition.x + radiusRandomNew);
            targetPosition.z = Random.Range(startPosition.z - radiusRandomNew, startPosition.z + radiusRandomNew);

            GameObject enemy = Instantiate(GameManager.Instance.enemyPrefab, startPosition, Quaternion.identity);
            enemy.transform.SetParent(GameManager.Instance.enemyParent.transform.parent, false);
            enemy.transform.GetChild(0).GetComponent<RagdollControl>().enemyState = RagdollControl.EnemyStates.random;
            enemy.transform.GetChild(0).GetComponent<RagdollControl>().radius = radiusRandom;
            enemy.transform.GetChild(0).GetComponent<RagdollControl>().startPosition = startPosition;
            enemy.transform.GetChild(0).GetComponent<RagdollControl>().speed = speedRandom;
            enemy.transform.GetChild(0).GetComponent<RagdollControl>().targetPosition = targetPosition;

            if (speedRandom != 0)
                enemy.transform.GetComponent<Animator>().SetTrigger("running");

        }
        point.Clear();
    }
    //************************************************************************Moving********************************************************************************************
    public void SpawnMovingEnemy(int numEnemy, int numOrbitEnemy, float radiusOrbit, float speed, bool barilInside)
    {
        List<float> enemyPosArray = new List<float>();
        float dxBorderL;
        float dxBorderR;


        int dxIndexEmptyPlace = 1000;
        int dzIndexEmptyPlace = 1000;

        float newRadiusOrbit;
        if (!barilInside)
            newRadiusOrbit = radiusOrbit + 2f;
        else
            newRadiusOrbit = radiusOrbit;

        Vector2 temp = FindEmptyPlace(numEnemy, newRadiusOrbit);

        dxIndexEmptyPlace = (int)temp.x;
        dzIndexEmptyPlace = (int)temp.y;

        if (dxIndexEmptyPlace < 1000)//there is Empty place
        {
            for (int pathPos = 0; pathPos < numEnemy; pathPos++)
            {

                enemyPosArray.Add(spawnPositionIdle[dxIndexEmptyPlace][dzIndexEmptyPlace + pathPos].z);

            }
            float dx = spawnPositionIdle[dxIndexEmptyPlace][dzIndexEmptyPlace].x;

            dxBorderL = spawnPositionIdle[dxIndexEmptyPlace - ((int)(radiusOrbit - 1) / 2)][dzIndexEmptyPlace].x;
            dxBorderR = spawnPositionIdle[dxIndexEmptyPlace + ((int)(radiusOrbit - 1) / 2)][dzIndexEmptyPlace].x;


            SpawnOrbitEnemyAlongTheRoute(numEnemy, numOrbitEnemy, radiusOrbit, speed, enemyPosArray, dx, dxBorderL, dxBorderR, barilInside);
        }
    }

    //******************************************************Spawn Moving Enemy

    public void SpawnOrbitEnemyAlongTheRoute(int numEnemy, int numOrbitEnemy, float radiusOrbit, float speedOrbit, List<float> enemyPos, float dx, float dxBarilL, float dxBarilR, bool barilInside)
    {
        List<GameObject> barilLeft = new List<GameObject>();
        List<GameObject> barilRight = new List<GameObject>();
        List<Vector3> point = new List<Vector3>();
        List<Vector3> barilPointL = new List<Vector3>();
        List<Vector3> barilPointR = new List<Vector3>();
        GameObject baril = new GameObject();
        List<GameObject> enemyOrbit = new List<GameObject>();
        float radiusOrbitNew;

        Vector3 p0 = GameManager.Instance.routes.GetChild(0).position;
        Vector3 p1 = GameManager.Instance.routes.GetChild(1).position;
        Vector3 p2 = GameManager.Instance.routes.GetChild(2).position;
        Vector3 p3 = GameManager.Instance.routes.GetChild(3).position;

        for (int i = 0; i < numEnemy; i++)
        {
            barilLeft.Add(null);
            barilRight.Add(null);
        }

        //************************Baril pointL
        for (int i = 0; i < numEnemy; i++)
        {
            p1.x = dxBarilL;
            p2.x = dxBarilL;
            float t1 = enemyPos[i];
            if (t1 < 1f)
            {
                barilPointL.Add(Mathf.Pow(1 - t1, 3) * p0 +
                        3 * Mathf.Pow(1 - t1, 2) * t1 * p1 +
                        3 * (1 - t1) * Mathf.Pow(t1, 2) * p2 +
                        Mathf.Pow(t1, 3) * p3);
            }
            var tempL = barilPointL[i];
            tempL.y = 0;
            barilPointL[i] = tempL;

            //Spawn bril in bourder
            if (!barilInside)
            {
                baril = Instantiate(GameManager.Instance.barilPrefab, barilPointL[i], Quaternion.identity);
                baril.transform.SetParent(GameManager.Instance.barilParent.transform.parent, false);
                var temp11 = baril.transform.position;
                temp11.y = 1f;
                baril.transform.position = temp11;

                barilLeft[i] = baril;
            }

        }



        //************************Baril pointR
        for (int i = 0; i < numEnemy; i++)
        {
            p1.x = dxBarilR;
            p2.x = dxBarilR;
            float t2 = enemyPos[i];
            if (t2 < 1f)
            {
                barilPointR.Add(Mathf.Pow(1 - t2, 3) * p0 +
                        3 * Mathf.Pow(1 - t2, 2) * t2 * p1 +
                        3 * (1 - t2) * Mathf.Pow(t2, 2) * p2 +
                        Mathf.Pow(t2, 3) * p3);
            }
            var tempR = barilPointR[i];
            tempR.y = 0;
            barilPointR[i] = tempR;

            //Spawn baril in bourder
            if (!barilInside)
            {
                baril = Instantiate(GameManager.Instance.barilPrefab, barilPointR[i], Quaternion.identity);
                baril.transform.SetParent(GameManager.Instance.barilParent.transform.parent, false);
                var temp22 = baril.transform.position;
                temp22.y = 1f;
                baril.transform.position = temp22;

                barilRight[i] = baril;
            }

        }



        for (int i = 0; i < numEnemy; i++)
        {

            p1.x = dx;
            p2.x = dx;
            float t = enemyPos[i];
            if (t < 1f)
            {
                point.Add(Mathf.Pow(1 - t, 3) * p0 +
                        3 * Mathf.Pow(1 - t, 2) * t * p1 +
                        3 * (1 - t) * Mathf.Pow(t, 2) * p2 +
                        Mathf.Pow(t, 3) * p3);
            }
            var temp = point[i];
            temp.y = 0;
            point[i] = temp;

            float teta0 = Random.Range(0f, 360f);
            float angle = 0f;


            for (int ii = 0; ii < numOrbitEnemy; ii++)
            {
                enemyOrbit.Add(null);
            }

            for (int j = 0; j < numOrbitEnemy; j++)
            {
                angle = j * Mathf.PI * 2f / numOrbitEnemy;

                if (!barilInside)
                    radiusOrbitNew = Mathf.Abs(barilPointR[i].x - barilPointL[i].x) / 2 - (Mathf.Abs(barilPointR[i].x - barilPointL[i].x) / 2) * 0.5f;
                else
                    radiusOrbitNew = Mathf.Abs(barilPointR[i].x - barilPointL[i].x) / 2 - (Mathf.Abs(barilPointR[i].x - barilPointL[i].x) / 2) * 0.2f;


                Vector3 pos = new Vector3(Mathf.Cos(angle + teta0 * Mathf.Deg2Rad) * radiusOrbitNew, 0f, Mathf.Sin(angle + teta0 * Mathf.Deg2Rad) * radiusOrbitNew) + point[i];

                enemyOrbit[j] = Instantiate(GameManager.Instance.enemyPrefab, pos, Quaternion.identity);
                enemyOrbit[j].transform.SetParent(GameManager.Instance.enemyParent.transform.parent, false);
                enemyOrbit[j].transform.GetChild(0).GetComponent<RagdollControl>().orbitCentre = point[i];

                enemyOrbit[j].transform.GetChild(0).GetComponent<RagdollControl>().enemyState = RagdollControl.EnemyStates.circular;
                enemyOrbit[j].transform.GetChild(0).GetComponent<RagdollControl>().speed = speedOrbit;

               
                if (speedOrbit != 0)
                {
                    enemyOrbit[j].transform.GetComponent<Animator>().SetTrigger("running");
                }
            }

            //Spawn bril in middle
            if (barilInside)
            {
                radiusOrbitNew = Mathf.Abs(barilPointR[i].x - barilPointL[i].x) / 2;
                baril = Instantiate(GameManager.Instance.barilPrefab, point[i], Quaternion.identity);
                baril.transform.SetParent(GameManager.Instance.barilParent.transform.parent, false);
                baril.transform.GetComponent<CapsuleCollider>().radius = radiusOrbitNew + radiusOrbitNew * 0.2f;
                var temp1 = baril.transform.position;
                temp1.y = 1f;
                baril.transform.position = temp1;
            }

        }

        //adjust radius of baril outside according to its z distance
        if (!barilInside)
        {
            for (int i = 0; i < numEnemy; i++)
            {
                radiusOrbitNew = Mathf.Abs(barilPointR[i].x - barilPointL[i].x) / 2;
                barilLeft[i].transform.GetComponent<CapsuleCollider>().radius = radiusOrbitNew + radiusOrbitNew * 0.2f;
                barilRight[i].transform.GetComponent<CapsuleCollider>().radius = radiusOrbitNew + radiusOrbitNew * 0.2f;
            }
        }

    }

    //************************************************************************Idle******************************************************************
    public void SpawnIdleEnemy(int numEnemy)
    {
        List<float> enemyPosArray = new List<float>();

        int dxIndexEmptyPlace = 1000;
        int dzIndexEmptyPlace = 1000;

        Vector2 temp = FindEmptyPlace(numEnemy, 1);

        dxIndexEmptyPlace = (int)temp.x;
        dzIndexEmptyPlace = (int)temp.y;

        if (dxIndexEmptyPlace < 1000)//there is Empty place
        {
            for (int pathPos = 0; pathPos < numEnemy; pathPos++)
            {

                enemyPosArray.Add(spawnPositionIdle[dxIndexEmptyPlace][dzIndexEmptyPlace + pathPos].z);
            }
            float dx = spawnPositionIdle[dxIndexEmptyPlace][dzIndexEmptyPlace].x;
            SpawnIdleEnemyAlongTheRoute(numEnemy, enemyPosArray, dx);
        }
    }
    //******************************************************Spawn Idle Enemy
    private void SpawnIdleEnemyAlongTheRoute(int numEnemy, List<float> enemyPos, float dx)
    {
        List<GameObject> enemy1 = new List<GameObject>();
        for (int i = 0; i < numEnemy; i++)
        {
            enemy1.Add(null);
        }
        Vector3 p0 = GameManager.Instance.routes.GetChild(0).position;
        Vector3 p1 = GameManager.Instance.routes.GetChild(1).position;
        p1.x = dx;
        Vector3 p2 = GameManager.Instance.routes.GetChild(2).position;
        p2.x = dx;
        Vector3 p3 = GameManager.Instance.routes.GetChild(3).position;

        for (int i = 0; i < numEnemy; i++)
        {
            float t = enemyPos[i];
            if (t < 1f)
            {
                enemy1[i] = Instantiate(GameManager.Instance.enemyPrefab);
                enemy1[i].transform.SetParent(GameManager.Instance.enemyParent.transform.parent, false);
                enemy1[i].transform.position = Mathf.Pow(1 - t, 3) * p0 +
                        3 * Mathf.Pow(1 - t, 2) * t * p1 +
                        3 * (1 - t) * Mathf.Pow(t, 2) * p2 +
                        Mathf.Pow(t, 3) * p3;
                var temp = enemy1[i].transform.position;
                temp.y = 0;
                enemy1[i].transform.position = temp;
            }

        }

    }


    //*******************************************************************************FindEmptyPlace*****************************************************************************
    Vector2 FindEmptyPlace(int numEmpty, float _radius)
    {
        int radius = (int)_radius;// 1, 3, 5, 7


        List<int> dxRandomList = new List<int>(dxIdle.Length);
        dxRandomList = RandomList(dxIdle.Length);
        int ii;

        int numEmptyZ = numEmpty;

        List<int> dzRandomList = new List<int>(dz.Length - numEmptyZ + 1);
        dzRandomList = RandomList(dz.Length - numEmptyZ + 1);
        int jj;

        int dxIndexEmptyPlace = 1000;
        int dzIndexEmptyPlace = 1000;

        for (int i = 0; i < dxRandomList.Count; i++)
        {
            ii = dxRandomList[i];
            for (int j = 0; j < dzRandomList.Count; j++)
            {
                jj = dzRandomList[j];

                bool pathStatus = false; // false: path is empty, True: path is full
                for (int pathPos = 0; pathPos < numEmptyZ; pathPos++)
                    if (spawnPositionIdle[ii][jj + pathPos].positionStatus)
                        pathStatus = true;

                bool insideBourder = true;//if inside bourder
                for (int index = 0; index < (radius - 1) / 2; index++)
                    if (index == ii || dxIdle.Length - 1 - index == ii)
                        insideBourder = false;
                if (radius == 9 && ii == 4)
                    insideBourder = true;




                if (!pathStatus && insideBourder)
                {
                    dxIndexEmptyPlace = ii;
                    dzIndexEmptyPlace = jj;
                    for (int pathPos = 0; pathPos < numEmptyZ; pathPos++)
                    {
                        for (int index = 0; index < (radius - 1) / 2 + 1; index++)
                        {
                            spawnPositionIdle[dxIndexEmptyPlace + index][dzIndexEmptyPlace + pathPos].positionStatus = true;
                            spawnPositionIdle[dxIndexEmptyPlace - index][dzIndexEmptyPlace + pathPos].positionStatus = true;

                        }

                    }

                    break;
                }

            }
            if (dxIndexEmptyPlace < 1000)
                break;
        }

        return new Vector2(dxIndexEmptyPlace, dzIndexEmptyPlace);
    }


    //*******************************************************************************RandomList*****************************************************************************
    List<int> RandomList(int sizeList)
    {
        //create array of random number for have random dx
        int randomNumber;
        List<int> TakeList = new List<int>();
        TakeList = new List<int>(new int[sizeList]);
        for (int i = 0; i < sizeList; i++)
        {
            randomNumber = UnityEngine.Random.Range(1, sizeList + 1);
            while (TakeList.Contains(randomNumber))
            {
                randomNumber = UnityEngine.Random.Range(1, sizeList + 1);
            }
            TakeList[i] = randomNumber;
        }

        for (int i = 0; i < sizeList; i++)
            TakeList[i] = TakeList[i] - 1;

        return TakeList;
    }


    //******************************************************************DefineParameterForEachDifficulty*********************************************************************
    public void DefineParameterForEachDifficulty()
    {
        //Level Parameters
        int A = 10;
        if (levelDifficulty < A)
        {
            numOrbitEnemyBarillInside = Mathf.FloorToInt(levelDifficulty / (A/2));//number of different path
            numOrbitBarillInside = 1;// number of orbit in each path
            numEnemyEachOrbitBarillInside = 2 + Mathf.FloorToInt(levelDifficulty % (A / 2));
            radiusOrbitBarillInside = 3f;
            speedOrbitBarillInside = 50f;// till 100

            numOrbitEnemyBarillOutside = Mathf.FloorToInt(levelDifficulty / A);
            numOrbitBarillOutside = 1;
            numEnemyEachOrbitBarillOutside = 5;
            radiusOrbitBarillOutside = 6f;
            speedOrbitBarillOutside = 100f;

            numRandomEnemy = 0;
            radiusRandomEnemy = 3f;
            speedRandomEnemy = 7f;

            numIdleEnemyPath = Mathf.FloorToInt(levelDifficulty %  2) + 1;
            numIdleEnemyEachPath = 3 + Mathf.FloorToInt(levelDifficulty / 2);
        }

        int B = 20;
        if ( levelDifficulty < B && levelDifficulty > A - 1)
        {
            numOrbitEnemyBarillInside = Mathf.FloorToInt(levelDifficulty / (B / 2)) + 1;//number of different path
            numOrbitBarillInside = 1;// number of orbit in each path
            numEnemyEachOrbitBarillInside = 2 + Mathf.FloorToInt(levelDifficulty % (B / 2));
            radiusOrbitBarillInside = 3f;
            speedOrbitBarillInside = 75f;// till 100

            numOrbitEnemyBarillOutside = Mathf.FloorToInt(levelDifficulty / (B / 2)); 
            numOrbitBarillOutside = 1;
            numEnemyEachOrbitBarillOutside = 5;
            radiusOrbitBarillOutside = 6f;
            speedOrbitBarillOutside = 75f;

            numRandomEnemy = levelDifficulty - 9;
            radiusRandomEnemy = 3f;
            speedRandomEnemy = 7f;

            numIdleEnemyPath = 0;
            numIdleEnemyEachPath = 3 + Mathf.FloorToInt(levelDifficulty / 2);
        }

        int C = 20;
        if (levelDifficulty < C && levelDifficulty > B - 1)
        {
            numOrbitEnemyBarillInside = Mathf.FloorToInt(levelDifficulty / (C / 2)) + 1;//number of different path
            numOrbitBarillInside = 1;// number of orbit in each path
            numEnemyEachOrbitBarillInside = 2 + Mathf.FloorToInt(levelDifficulty % (B / 2));
            radiusOrbitBarillInside = 3f;
            speedOrbitBarillInside = 100f;// till 100

            numOrbitEnemyBarillOutside = Mathf.FloorToInt(levelDifficulty / (B / 2));
            numOrbitBarillOutside = 1;
            numEnemyEachOrbitBarillOutside = 5;
            radiusOrbitBarillOutside = 6f;
            speedOrbitBarillOutside = 100f;

            numRandomEnemy = levelDifficulty - 9;
            radiusRandomEnemy = 3f;
            speedRandomEnemy = 10f;

            numIdleEnemyPath = 0;
            numIdleEnemyEachPath = 3 + Mathf.FloorToInt(levelDifficulty / 2);
        }
    }
    //************************************************************************************************************************************************************************

    public void TestForWin()
    {
        for (int i = 1; i < enemyParent.transform.parent.childCount; i++)
        {
            if (enemyParent.transform.parent.GetChild(i).GetChild(0).GetComponent<RagdollControl>().isAlive)
            {
                return;
            }
        }
        StartCoroutine(VictoryEffect());
    }

    IEnumerator VictoryEffect()
    {
        notWinYet = false;
        yield return new WaitForSeconds(0.5f);
        GameObject ve = Instantiate(victoryEffect);
        ve.transform.SetParent(playerParent.transform, false);
        yield return new WaitForSeconds(0.2f);
        _player.myState = Player.PlayerStates.Celebration;
        cam.GetComponent<Animator>().SetTrigger("celebration");
        yield return new WaitForSeconds(2f);
        GameManager.Instance.scoreUpdated = true;
        yield return new WaitForSeconds(8f);
        NextLevel();
    }

    public void NextLevel()
    {
        if (GameConstants.LEVEL < 31)
        {
            PlayerPrefs.SetInt("LEVEL", GameConstants.LEVEL);
            PlayerPrefs.SetInt("SCORE", GameConstants.SCORE);
            GameConstants.SHOT = 0;
        }
        else
        {
            PlayerPrefs.SetInt("LEVEL", 0);
            PlayerPrefs.SetInt("SCORE", 0);
            GameConstants.SHOT = 0;

        }

        LevelChanger.fadeToNextLevel = true;
    }

    public void PreviousLevel()
    {
        if (GameConstants.LEVEL > 1)
            PlayerPrefs.SetInt("LEVEL", GameConstants.LEVEL - 2);
        else
            PlayerPrefs.SetInt("LEVEL", 0);


        LevelChanger.fadeToNextLevel = true;
    }

    public void ResetLevel()
    {
        PlayerPrefs.DeleteKey("LEVEL");
        PlayerPrefs.DeleteKey("SCORE");
        GameConstants.SHOT = 0;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}

[System.Serializable]
public class SpawnPosition
{
    public float x;
    public float z;
    public bool positionStatus;

    public SpawnPosition(float _x, float _z, bool _positionStatus)
    {
        x = _x;
        z = _z;
        positionStatus = _positionStatus;
    }
}