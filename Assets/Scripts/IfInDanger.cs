﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IfInDanger : MonoBehaviour
{
    public float powerShoot = 50.0f;
    public float powerExplosion = 3.0f;
    public Vector3 explosionPosition;
    public float radiusExplosion = 10.0f;
    private float upForceExplosion = 0.8f;




    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        Rigidbody[] rb = this.transform.parent.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody _rb in rb)
        {
            _rb.AddExplosionForce(powerExplosion, explosionPosition, radiusExplosion, upForceExplosion, ForceMode.Impulse);
        }
    }

    public void KillEnemyAoundBarill()
    {
        this.transform.parent.GetChild(0).GetComponent<RagdollControl>().isAlive = false;

        this.GetComponentInParent<Animator>().enabled = false;

        if (this.transform.parent.GetChild(0).GetComponent<RagdollControl>())
            this.transform.parent.GetChild(0).GetComponent<RagdollControl>().ActivateRagdoll();
        this.GetComponentInParent<Animator>().SetTrigger("dying");

        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

        if (GameManager.Instance.notWinYet)
            GameManager.Instance.TestForWin();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("DangerPath") && this.transform.parent.GetChild(0).GetComponent<RagdollControl>().isAlive)
        {
            this.transform.GetComponent<SkinnedMeshRenderer>().material = GameManager.Instance.enemyOrangeMaterial;
        }

        if (col.gameObject.tag == "Arrow")
        {
            if (this.transform.parent.GetChild(0).GetComponent<RagdollControl>())
                this.transform.parent.GetChild(0).GetComponent<RagdollControl>().ActivateRagdoll();
            //Destroy(col.gameObject);
            this.GetComponentInParent<Animator>().SetTrigger("dying");

            if (this.transform.parent.childCount == 3)
                this.transform.parent.GetChild(2).GetComponent<ParticleSystem>().Play();


            this.transform.parent.GetChild(0).GetComponent<RagdollControl>().isAlive = false;

            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
            this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            Rigidbody[] rb = this.transform.parent.GetComponentsInChildren<Rigidbody>();
            foreach (Rigidbody _rb in rb)
            {
                _rb.AddForce(this.transform.position.normalized * powerShoot, ForceMode.Impulse);
            }

            GameManager.Instance.TestForWin();

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("DangerPath"))
        {
            this.transform.GetComponent<SkinnedMeshRenderer>().material = GameManager.Instance.enemyRedMaterial;
        }
    }
}
