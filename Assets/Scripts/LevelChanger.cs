﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    public Animator animator;
    public static bool fadeToNextLevel = false;


    // Start is called before the first frame update
    void Start()
    {
        fadeToNextLevel = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (fadeToNextLevel)
        {
            FadeToNextLevel();
            fadeToNextLevel = false;
        }
    }
    public void FadeToNextLevel()
    {
        animator.SetTrigger("fadeout");

    }

    public void OnFadeComplete()
    {
        animator.SetTrigger("fadein");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
}
