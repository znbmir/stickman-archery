﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDesign : MonoBehaviour
{
    public bool testMode;
    public AnimationCurve lineSplitDifficulty;
    public int fakeGameLevel;
    // Start is called before the first frame update

    public int GetLineSplitDifficulty()
    {
        if (testMode)
            return Mathf.FloorToInt(lineSplitDifficulty.Evaluate(fakeGameLevel));
        else
            return Mathf.FloorToInt(lineSplitDifficulty.Evaluate(GameConstants.LEVEL));
    }
}
