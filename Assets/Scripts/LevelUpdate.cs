﻿using UnityEngine;
using TMPro;

public class LevelUpdate : MonoBehaviour
{
    public TextMeshProUGUI textNum_Level;
    public TextMeshProUGUI textNum_Score;


    // Start is called before the first frame update
    void Start()
    {
        textNum_Level.text = "" + GameConstants.LEVEL;
        textNum_Score.text = "" + GameConstants.SCORE;

    }

}
