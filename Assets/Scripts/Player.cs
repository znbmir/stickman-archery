﻿using System.Collections;
using TMPro;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float speed;
    public float rotationPower;
    private Vector3 point, pointPreviousPos;
    private Camera cam;
    private Vector2 mousePos = new Vector2();
    private Vector3 delta = new Vector3();
    public GameObject player;
    public GameObject[] pathConrolPoints;
    public Animator PlayerAnim;
    public TextMeshProUGUI coinNumPeafab;



    public enum PlayerStates
    {
        Idle, ShootArrow, Celebration
    }

    public PlayerStates myState;

    // Start is called before the first frame update
    void Awake()
    {
        myState = PlayerStates.Idle;
        cam = Camera.main;


    }

    // Update is called once per frame
    void Update()
    {
        HandleMovement();
    }

    private void HandleMovement()
    {
        if (myState == PlayerStates.Idle)
        {

            if (Input.GetMouseButtonDown(0))
            {
                mousePos = Input.mousePosition;
                point = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10f));
            }
            if (Input.GetMouseButton(0))
            {
                pointPreviousPos = point;
                mousePos = Input.mousePosition;
                point = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10f));
                delta.x = point.x - pointPreviousPos.x;
                rotationPower += delta.x * speed;
                if(Mathf.Abs(delta.x) > 0)
                    DrawRoute.pointsMoved = true;

                foreach (GameObject obj in GameManager.Instance.pathObjMoving)
                    obj.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;

                foreach (GameObject obj in GameManager.Instance.pathObj)
                    obj.transform.GetChild(0).GetComponent<CapsuleCollider>().enabled = true;
            }

            if (Input.GetMouseButtonUp(0) && GameManager.Instance.hasArrow)
            {
               foreach (GameObject obj in GameManager.Instance.pathObjMoving)
                   obj.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

                foreach (GameObject obj in GameManager.Instance.pathObj)
                    obj.transform.GetChild(0).GetComponent<CapsuleCollider>().enabled = false;

                ArrowBezierFollow.shootAllowed = true;
                GameManager.Instance.arrow.transform.SetParent(GameManager.Instance.arrowMovingParent.transform.parent, false);

                GameConstants.SHOT++;
                if (GameConstants.SHOT < 2)
                    GameManager.Instance.shootTxt.GetComponent<TextMeshProUGUI>().text = "shot";
                else
                    GameManager.Instance.shootTxt.GetComponent<TextMeshProUGUI>().text = "shots";
                PlayerAnim.SetTrigger("shoot");
                GameManager.Instance.shootTxt.gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "" + GameConstants.SHOT;
                GameManager.Instance.shootTxt.GetComponent<Animator>().SetTrigger("shoot");
                GameManager.Instance.instantiateArrow = true;
                GameManager.Instance.hasArrow = false;

            }


            
            rotationPower = Mathf.Lerp(rotationPower, 0, Time.deltaTime * 30);
            player.transform.Rotate(0, rotationPower, 0);
            pathConrolPoints[0].transform.Translate(rotationPower, 0, 0);
            pathConrolPoints[1].transform.Translate(rotationPower, 0, 0);
        }

        if (myState == PlayerStates.Celebration)
        {
            StartCoroutine(BecomeHappy());
            Destroy(GameManager.Instance.arrow.gameObject);
        }





    }


    IEnumerator BecomeHappy()
    {
        yield return new WaitForSeconds(0.1f);
        PlayerAnim.SetTrigger("dance");

    }

}
