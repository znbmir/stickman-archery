﻿using UnityEngine;

public class RagdollControl : MonoBehaviour
{
    private Animator anim;
    private Rigidbody playerRb;
    private CapsuleCollider capsuleCollider;

    private Rigidbody[] rigidbodies;
    private Collider[] colliders;
    private ParticleSystem ps;
    private Quaternion defaultRotation;
    public bool isAlive = true;
    public EnemyStates enemyState;
    public Vector3 orbitCentre;
    public float speed;
    private Vector3 prevPosition;

    private bool arrived;
    public Vector3 targetPosition;
    public Vector3 startPosition;
    public float radius = 5f;
    private Vector3 movementDir;



    public enum EnemyStates
    {
        Idle, circular, random
    }


    private void Awake()
    {
        enemyState = EnemyStates.Idle;
        anim = this.transform.parent.GetComponent<Animator>();
        playerRb = this.transform.parent.GetChild(1).GetComponent<Rigidbody>();
        capsuleCollider = this.transform.parent.GetChild(1).GetComponent<CapsuleCollider>();
        defaultRotation = this.transform.parent.GetChild(1).GetComponent<Rigidbody>().rotation;
        rigidbodies = GetComponentsInChildren<Rigidbody>();
        colliders = GetComponentsInChildren<Collider>();

        SetCollidersEnabled(false);
        SetRigidbodieskinematic(true);
        ps = this.transform.parent.GetChild(2).GetComponent<ParticleSystem>();

        if (enemyState == EnemyStates.random)
        {
            movementDir = targetPosition - this.transform.parent.transform.position;
            arrived = false;
        }

    }

    private void Update()
    {
        if (ps)
        {
            if (!isAlive && !ps.IsAlive())
            {
                Destroy(ps.gameObject);
            }
        }

        if (isAlive)
        {
            this.transform.parent.GetChild(1).GetComponent<Rigidbody>().transform.position = this.transform.parent.transform.position;
            this.transform.parent.GetChild(1).GetComponent<Rigidbody>().rotation = defaultRotation;
        }

        if (!Input.GetMouseButton(0))
        {
            this.transform.parent.GetChild(1).GetComponent<SkinnedMeshRenderer>().material = GameManager.Instance.enemyRedMaterial;
        }

            if (enemyState == EnemyStates.circular && isAlive)
        {
            prevPosition = this.transform.parent.transform.position;
            this.transform.parent.transform.RotateAround(orbitCentre, Vector3.up, speed * Time.deltaTime);
            this.transform.parent.transform.rotation = Quaternion.LookRotation(this.transform.parent.transform.position - prevPosition);
        }

        if (enemyState == EnemyStates.random && isAlive)
        {
            if (!arrived)
                RandomMove();

            if (this.transform.parent.transform.position == targetPosition)
            {
                targetPosition.x = Random.Range(startPosition.x - radius, startPosition.x + radius);
                targetPosition.z = Random.Range(startPosition.z - radius, startPosition.z + radius);
                movementDir = targetPosition - this.transform.parent.transform.position;
                arrived = false;
            }
        }
        

    }

    void RandomMove()
    {
        float singleStep = speed * Time.deltaTime;
        Vector3 newDirection = Vector3.RotateTowards(this.transform.parent.transform.forward, movementDir, singleStep, 0.0f);
        this.transform.parent.transform.rotation = Quaternion.LookRotation(newDirection);
        this.transform.parent.transform.position = Vector3.MoveTowards(this.transform.parent.transform.position, targetPosition, singleStep);
        if ((targetPosition - transform.position).magnitude < 0.01f)
        {
            this.transform.parent.transform.position = targetPosition;
            arrived = true;
        }
    }

    private void SetCollidersEnabled(bool enabled)
    {
        foreach(Collider col in colliders)
        {
            col.enabled = enabled;
        }
    }

    private void SetRigidbodieskinematic (bool kinematic)
    {
        foreach(Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = kinematic;
        }
    }

    public void ActivateRagdoll()
    {
        capsuleCollider.enabled = false;
        playerRb.isKinematic = true;
        anim.enabled = false;
        SetCollidersEnabled(true);
        SetRigidbodieskinematic(false);
    }

}
