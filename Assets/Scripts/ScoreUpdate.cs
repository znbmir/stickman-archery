﻿using UnityEngine;
using TMPro;
using DG.Tweening;
using System.Collections.Generic;

public class ScoreUpdate : MonoBehaviour
{
    public TextMeshProUGUI textNum_Object;
    int targetScore;
    bool targetScoreUpdated;

    public TextMeshProUGUI coinNumPrefab;
    TextMeshProUGUI coinNum;

    public GameObject coinNumCanvas;
    [SerializeField] GameObject animatedCoinPrefab;
    [SerializeField] int maxCoins;
    [SerializeField] [Range(0.5f, 0.9f)] float minAnimDuration;
    [SerializeField] [Range(0.9f, 2f)] float maxAnimDuration;
    Queue<GameObject> coinsQueue = new Queue<GameObject>();

    [SerializeField] Ease easeType;

    private bool saidCongrats;


    void PrepareCoins()
    {
        GameObject coin;
        for(int i = 0; i < maxCoins; i++)
        {
            coin = Instantiate(animatedCoinPrefab);
            coin.transform.SetParent(GameManager.Instance.coinNumCanvas.transform, false);
            coin.SetActive(false);
            coinsQueue.Enqueue(coin);

        }
    }

    void AnimateCoin(int amount)
    {
        for(int i = 0; i < amount; i++)
        {
            if(coinsQueue.Count > 0)
            {
                GameObject coin = coinsQueue.Dequeue();
                coin.SetActive(true);
                coin.transform.position = coinNum.transform.position;
                float duration = Random.Range(minAnimDuration, maxAnimDuration);
                coin.transform.DOMove(this.transform.position, duration)
                    .SetEase(easeType)
                    .OnComplete(() => {
                        coin.SetActive(false);
                        coinsQueue.Enqueue(coin);
                        GameConstants.SCORE++;
                        textNum_Object.text = "" + GameConstants.SCORE;
                        Destroy(coinNum);
                    });
            }
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        textNum_Object.text = "" + GameConstants.SCORE;
        targetScoreUpdated = false;
        PrepareCoins();
        saidCongrats = false;
    }

    void Update()
    {
        if(!GameManager.Instance.notWinYet && !saidCongrats)
        {
            if(GameConstants.SHOT == GameConstants.ARROWGOLDEN || GameConstants.SHOT < GameConstants.ARROWGOLDEN)
                GameManager.Instance.greatTxt.GetComponent<Animator>().SetTrigger("come");
            else if(GameConstants.SHOT == GameConstants.ARROWSILVER || GameConstants.SHOT < GameConstants.ARROWSILVER)
                GameManager.Instance.goodTxt.GetComponent<Animator>().SetTrigger("come");
            else if (GameConstants.SHOT == GameConstants.ARROWBRONZE || GameConstants.SHOT < GameConstants.ARROWBRONZE)
                GameManager.Instance.fineTxt.GetComponent<Animator>().SetTrigger("come");

            saidCongrats = true;

        }

        if (GameManager.Instance.scoreUpdated)
        {

            if (!targetScoreUpdated && (GameConstants.SHOT == GameConstants.ARROWGOLDEN || GameConstants.SHOT < GameConstants.ARROWGOLDEN))
            {
                targetScore = (GameConstants.ARROWGOLDEN + 1) * 10;
                targetScoreUpdated = true;
                coinNum = Instantiate(coinNumPrefab);
                coinNum.text = "+" + (GameConstants.ARROWGOLDEN + 1) * 10;
                coinNum.transform.SetParent(GameManager.Instance.coinNumCanvas.transform, false);
                AnimateCoin(targetScore);
            }

            else if (!targetScoreUpdated && (GameConstants.SHOT == GameConstants.ARROWSILVER || GameConstants.SHOT < GameConstants.ARROWSILVER))
            {
                targetScore = (GameConstants.ARROWGOLDEN) * 10;
                targetScoreUpdated = true;
                //Destroy(Instantiate(coinNumPrefab, GameManager.Instance.playerParent.transform.position, Quaternion.identity), 1f);
                coinNum = Instantiate(coinNumPrefab);
                coinNum.text = "+" + (GameConstants.ARROWGOLDEN) * 10;
                coinNum.transform.SetParent(GameManager.Instance.coinNumCanvas.transform, false);
                AnimateCoin(targetScore);
            }

            else if (!targetScoreUpdated && (GameConstants.SHOT == GameConstants.ARROWBRONZE || GameConstants.SHOT < GameConstants.ARROWBRONZE))
            {
                targetScore = (GameConstants.ARROWGOLDEN - 1) * 10;
                targetScoreUpdated = true;
                coinNum = Instantiate(coinNumPrefab);
                coinNum.transform.SetParent(GameManager.Instance.coinNumCanvas.transform, false);
                coinNum.text = "+" + (GameConstants.ARROWGOLDEN - 1) * 10;
                AnimateCoin(targetScore);
                print(targetScore);
            }

            //   GameConstants.SCORE = (int)Mathf.MoveTowards(GameConstants.SCORE, targetScore, GameConstants.COUNTSPEED * Time.deltaTime);
            //   textNum_Object.text = "" + GameConstants.SCORE;
            if (GameConstants.SCORE == targetScore || GameConstants.SCORE > targetScore)
            {
                GameManager.Instance.scoreUpdated = false;
            }

        }
    }

}