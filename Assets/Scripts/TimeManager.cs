﻿using System.Collections;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    float slowmotionScale = 0.5f;
    public bool slowmotionFinished = false;

    private void Start()
    {

    }

    private void Update()
    {
        if(slowmotionFinished)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 1, Time.deltaTime);
            Time.fixedDeltaTime = 0.02F * Time.timeScale;
        }

        if(Mathf.Abs(Time.timeScale - 1) < 0.01)
        {
            Time.timeScale = 1;
            slowmotionFinished = false;
        }
    }

    public IEnumerator DoSlowmotion()
    {
        slowmotionFinished = false;
        Time.timeScale = slowmotionScale;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        yield return new WaitForSecondsRealtime(2f);
        slowmotionFinished = true;
    }
}
